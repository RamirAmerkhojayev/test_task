#!/bin/bash

#install nginx in ubuntu from apt repository
sudo apt update
sudo apt install nginx -y
nginx -v

#create html page
touch /var/www/html/index.html
cat ./index.html > /var/www/html/index.html

#create configuration file for nginx server
touch /etc/nginx/conf.d/web-site.conf
cat ./wev-site.conf > /etc/nginx/conf.d/web-site.conf

#check configuration file for errors and start nginx service
nginx -t
systemctl restart nginx
systemctl status nginx
