#!/bin/bash

#build my docker image from dockerfile
docker build -t nginx:1.0 .

#run our docker container
docker run -d -p 8080:80 --name webserver nginx:1.0