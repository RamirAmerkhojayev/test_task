FROM ubuntu:latest

RUN apt update -y && apt install nginx -y

COPY index.html /var/www/html/index.html
COPY web-site.conf /etc/nginx/conf.d/web-site.conf

EXPOSE 80

CMD [ "/usr/sbin/nginx", "-g", "daemon off;" ]